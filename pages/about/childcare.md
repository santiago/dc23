---
name: Child care
---
# Child care
Child care facility inside the Infopark Kochi campus can be used by DebConf23
attendees, please make sure to request in advance so that we can make necessary
arrangements. And these requests are expected to be made before July 2023.

## Kid's Day Care Centre at Infopark Campus
Infopark Phase 1 campus at Kochi has a kid’s day care centre/creche and play
school facility available. The centre with a built-up area of 7000 sq.ft with
all the facilities can accommodate up to 160 kids. Sydney Montessori Schools is
managing this facility for Infopark. They have over 16 years of experience in
running Montessori schools, 17 centres across 6 districts in Kerala. Working
professionals can make use of this facility inside the campus.

## Services
They take care of children with age from 6 months to 6 years. Different sections are as described below.

| Section   | Age           |
|-----------|---------------|
| Baby Care | 6 m - 1.5 Y   |
| Baby Sit	| 1.5 Y - 2.5 Y |
| Play Class| 2.5 Y - 4 Y   |
| LKG	    | 4 Y - 5 Y     |
| UKG	    | 5 Y - 6 Y     |


## Working Time
- Working Time: 08:00 AM to 06:30 PM
- Working days: All days except Sundays and on Force Majeure occasions.

## Contact
- Mr. Joseph K: +91-9388708232, +91-9447148015
